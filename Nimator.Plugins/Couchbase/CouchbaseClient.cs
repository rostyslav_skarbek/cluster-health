﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Nimator.Plugins.Couchbase
{
    /// <summary>
    /// 
    /// </summary>
    public class CouchbaseClient : ICouchbaseClient
    {
        static readonly HttpClient Client = new HttpClient();
        private const string Path = "pools/default";

        /// <summary>
        /// Constructor to create Couchbase client.
        /// </summary>
        /// <param name="host">Couchbase host.</param>
        /// <param name="userName">Couchbase user.</param>
        /// <param name="password">Couchbase password.</param>
        public CouchbaseClient(string host, string userName, string password)
        {
            InitialiseClient(host, userName, password);
        }

        /// <inheritDoc/>
        public async Task<CouchbaseClusterDetails> GetClusterDetailsAsync()
        {
            var response = await Client.GetAsync(Path);
            if (!response.IsSuccessStatusCode)
            {
                throw new ApplicationException(
                    $"Error while getting cluster details: Response code: {response.StatusCode}; Reason {response.ReasonPhrase}");
            }
            return await response.Content.ReadAsAsync<CouchbaseClusterDetails>();
        }

        private void InitialiseClient(string host, string userName, string password)
        {
            Client.BaseAddress = new Uri(host);
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var byteArray = Encoding.ASCII.GetBytes($"{userName}:{password}");
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            Client.DefaultRequestHeaders.Add("X-memcachekv-Store-Client-Specification-Version", "0.1");
        }
    }
}
