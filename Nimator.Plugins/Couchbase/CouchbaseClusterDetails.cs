﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nimator.Plugins.Couchbase
{
    public class CouchbaseClusterDetails
    {
        [JsonProperty(PropertyName = "nodes")]
        public List<Node> Nodes { get; set; }

        [JsonProperty(PropertyName = "clusterName")]
        public string ClusterName { get; set; }
    }
    public class InterestingStats
    {
        [JsonProperty(PropertyName = "curr_items")]
        public int? CurrItems { get; set; }

        [JsonProperty(PropertyName = "curr_items_tot")]
        public int? CurrItemsTot { get; set; }
    }

    public class Node
    {
        [JsonProperty(PropertyName = "interestingStats")]
        public InterestingStats InterestingStats { get; set; }

        [JsonProperty(PropertyName = "memoryTotal")]
        public long? MemoryTotal { get; set; }

        [JsonProperty(PropertyName = "memoryFree")]
        public long? MemoryFree { get; set; }
    }
}