﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Nimator.Plugins.Couchbase
{
    /// <summary>
    /// Check that monitor Couchbase cluster health.
    /// </summary>
    public class CouchbaseClusterHealthCheck : ICheck
    {
        private readonly ICouchbaseClient _client;
        private readonly float _minAvailableMemoryRatio;
        private readonly int _maximumDocumentsThreshold;

        /// <inheritDoc/>
        public string ShortName { get; } = nameof(CouchbaseClusterHealthCheck);

        /// <summary>
        /// Constructs health check for cluster.
        /// </summary>
        /// <param name="minAvailableMemoryRatio">Minimum available memory ratio.</param>
        /// <param name="maximumDocumentsThreshold">Maximum documents threshold.</param>
        /// <param name="client">Couchbase client.</param>
        public CouchbaseClusterHealthCheck(float minAvailableMemoryRatio, int maximumDocumentsThreshold, ICouchbaseClient client)
        {
            if (minAvailableMemoryRatio < 0.0F || minAvailableMemoryRatio > 1.0F)
                throw new ArgumentException("Minimum available memory ratio is not valid.", nameof(minAvailableMemoryRatio));
            if (maximumDocumentsThreshold < 0)
                throw new ArgumentException("Maximum documents threshold is less than 0.", nameof(maximumDocumentsThreshold));

            _client = client ?? throw new ArgumentNullException(nameof(client));
            _minAvailableMemoryRatio = minAvailableMemoryRatio;
            _maximumDocumentsThreshold = maximumDocumentsThreshold;
        }

        /// <summary>
        /// Returns <see cref="ICheckResult"/> after checking cluster health.
        /// </summary>
        /// <returns></returns>
        public async Task<ICheckResult> RunAsync()
        {
            try
            {
                var clusterDetails = await _client.GetClusterDetailsAsync();
                return CheckClusterHealth(clusterDetails);
            }
            catch (Exception ex)
            {
                return new CheckResult(ShortName, NotificationLevel.Error, $"Error while checking cluster health. Details: {ex.Message}");
            }
        }

        private CheckResult CheckClusterHealth(CouchbaseClusterDetails clusterDetails)
        {
            if (!CanGetMemmoryInfo(clusterDetails))
                return new CheckResult(ShortName, NotificationLevel.Error, "Cannot get information about the memory of the cluster.");

            if (!CanGetDocumentsInfo(clusterDetails))
                return new CheckResult(ShortName, NotificationLevel.Error, "Cannot get information about the documents at the cluster.");

            var isEnoughMemory = clusterDetails.Nodes.All(node=> (double)node.MemoryFree.GetValueOrDefault() / node.MemoryTotal > _minAvailableMemoryRatio);
            var isDocumentNumberWithinLimit = clusterDetails.Nodes.All(node => node.InterestingStats.CurrItemsTot <= _maximumDocumentsThreshold);
            
            if (isEnoughMemory && isDocumentNumberWithinLimit)
            {
                return new CheckResult(ShortName, NotificationLevel.Okay,
                    $"Cluster: {clusterDetails.ClusterName} is healthy.");
            }

            return new CheckResult(ShortName, NotificationLevel.Warning,
                $"Cluster: {clusterDetails.ClusterName} is not healthy.");
        }

        private bool CanGetMemmoryInfo(CouchbaseClusterDetails clusterDetails)
        {
            return clusterDetails?.Nodes != null
                   && clusterDetails.Nodes?.Count > 0
                   && clusterDetails.Nodes[0].MemoryTotal.HasValue
                   && clusterDetails.Nodes[0].MemoryFree.HasValue
                   && clusterDetails.Nodes[0].MemoryTotal > 0;
        }

        private bool CanGetDocumentsInfo(CouchbaseClusterDetails clusterDetails)
        {
            return clusterDetails?.Nodes != null
                   && clusterDetails.Nodes?.Count > 0 
                   && clusterDetails.Nodes[0].InterestingStats?.CurrItemsTot != null;
        }
    }
}