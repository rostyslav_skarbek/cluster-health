﻿namespace Nimator.Plugins.Couchbase
{
    /// <summary>
    /// Settings for checks that monitor Couchbase cluster health
    /// </summary>
    public class CouchbaseClusterHealthCheckSettings : ICheckSettings
    {
        /// <summary>
        /// Seetings for connection to Couchbase cluster.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// User name to connect to couchbase.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Pasword to connect to couchbase.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Minimum available memory ratio (from 0.0 to 1.0)
        /// </summary>
        public float MinAvailableMemoryRatio { get; set; }

        /// <summary>
        /// Maximum documents threshold.
        /// </summary>
        public int MaximumDocumentsThreshold { get; set; }


        /// <inheritDoc/>
        public ICheck ToCheck()
        {
            return new CouchbaseClusterHealthCheck(MinAvailableMemoryRatio, MaximumDocumentsThreshold, new CouchbaseClient(Host, UserName, Password));
        }
    }
}