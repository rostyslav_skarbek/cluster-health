﻿using System.Threading.Tasks;

namespace Nimator.Plugins.Couchbase
{
    /// <summary>
    /// Couchbase client.
    /// </summary>
    public interface ICouchbaseClient
    {
        /// <summary>
        /// Get cluster details asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<CouchbaseClusterDetails> GetClusterDetailsAsync();
    }
}