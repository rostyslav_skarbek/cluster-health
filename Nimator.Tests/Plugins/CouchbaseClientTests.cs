﻿using System;
using Nimator.Plugins.Couchbase;
using NUnit.Framework;

namespace Nimator.Plugins
{
    [TestFixture]
    public class CouchbaseClientTests
    {
        [Test]
        public void Constructor_ValidSettings_CreateInstanceOfCouchbaseClient()
        {
            var client = new CouchbaseClient("http://127.0.0.1:9090", "user", "password");

            Assert.NotNull(client);
            Assert.IsInstanceOf<CouchbaseClient>(client);
        }

        [Test]
        public void Constructor_InvalidHost_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new CouchbaseClient("fake:9090", "user", "password"));
        }
    }
}
