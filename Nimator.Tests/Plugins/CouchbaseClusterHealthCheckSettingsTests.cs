﻿using System;
using Nimator.Plugins.Couchbase;
using NUnit.Framework;

namespace Nimator.Plugins
{
    [TestFixture]
    public class CouchbaseClusterHealthCheckSettingsTests
    {
        [Test]
        public void ValidSettings_ToCheck_RetunrnsCouchbaseClusterHealthCheck()
        {
            // Arrange
            var checkSettings = new CouchbaseClusterHealthCheckSettings
            {
                Host = "http://127.0.0.1",
                UserName = "user",
                Password = "password",
                MinAvailableMemoryRatio = 0.15F,
                MaximumDocumentsThreshold = 10000
            };

            // Act
            var check = checkSettings.ToCheck();
            
            // Assert
            Assert.NotNull(check);
            Assert.IsInstanceOf<CouchbaseClusterHealthCheck>(check);
        }
    }
}
