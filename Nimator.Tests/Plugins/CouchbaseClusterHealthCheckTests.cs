﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using Nimator.Plugins.Couchbase;
using NUnit.Framework;

namespace Nimator.Plugins
{
    [TestFixture]
    public class CouchbaseClusterHealthCheckTests
    {
        [TestCase(-1.0F)]
        [TestCase(1.1F)]
        [TestCase(-0.001F)]
        public void Constructor_WhenPassedInvalidMemoryRatio_ThrowsException(float minAvailableMemoryRatio)
        {
            var clientMock = new Mock<ICouchbaseClient>();
            var exception = Assert.Throws<ArgumentException>(() => new CouchbaseClusterHealthCheck(minAvailableMemoryRatio, 100000, clientMock.Object));
            Assert.AreEqual("minAvailableMemoryRatio", exception.ParamName);
        }

        [TestCase(-1)]
        [TestCase(-100000)]
        public void Constructor_WhenPassedInvalidDocumentsThreshold_ThrowsException(int maximumDocumentsThreshold)
        {
            var clientMock = new Mock<ICouchbaseClient>();
            var exception = Assert.Throws<ArgumentException>(() => new CouchbaseClusterHealthCheck(0.20F, maximumDocumentsThreshold, clientMock.Object));
            Assert.AreEqual("maximumDocumentsThreshold", exception.ParamName);
        }

        [Test]
        public void Constructor_WhenPassedNullArgument_ThrowsException()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => new CouchbaseClusterHealthCheck(0.20F, 10000, null));
            Assert.AreEqual("client", exception.ParamName);
        }

        [Test]
        public async Task MemoryAndDocumentsAreOk_WhenRun_ReturnsOk()
        {
            //Arrange
            var goodMemoryRatio = 0.25F;
            var documentsThreshold = 100000;
            var clientMock = new Mock<ICouchbaseClient>();
            clientMock.Setup(m => m.GetClusterDetailsAsync()).ReturnsAsync(GetValidClusterDetails(goodMemoryRatio, documentsThreshold));

            var check = new CouchbaseClusterHealthCheck(goodMemoryRatio, documentsThreshold, clientMock.Object);

            //Act
            var result = await check.RunAsync();

            //Assert
            Assert.AreEqual(NotificationLevel.Okay, result.Level);
            Assert.AreEqual(nameof(CouchbaseClusterHealthCheck), result.CheckName);
        }

        [Test]
        public async Task CouchbaseClientThrowsAnException_WhenRun_ReturnsError()
        {
            //Arrange
            var goodMemoryRatio = 0.25F;
            var documentsThreshold = 100000;
            var clientMock = new Mock<ICouchbaseClient>();
            clientMock.Setup(m => m.GetClusterDetailsAsync())
                .ThrowsAsync(new ApplicationException("Some error"));

            var check = new CouchbaseClusterHealthCheck(goodMemoryRatio, documentsThreshold, clientMock.Object);

            //Act
            var result = await check.RunAsync();

            //Assert
            Assert.AreEqual(NotificationLevel.Error, result.Level);
            Assert.AreEqual(nameof(CouchbaseClusterHealthCheck), result.CheckName);
        }

        [Test]
        public async Task NodeInfoIsMissing_WhenRun_ReturnsError()
        {
            //Arrange
            var goodMemoryRatio = 0.25F;
            var documentsThreshold = 100000;
            var clientMock = new Mock<ICouchbaseClient>();
            var clusterInfo = GetValidClusterDetails(goodMemoryRatio, documentsThreshold);
            clusterInfo.Nodes = null;
            clientMock.Setup(m => m.GetClusterDetailsAsync()).ReturnsAsync(clusterInfo);

            var check = new CouchbaseClusterHealthCheck(goodMemoryRatio, documentsThreshold, clientMock.Object);

            //Act
            var result = await check.RunAsync();

            //Assert
            Assert.AreEqual(NotificationLevel.Error, result.Level);
            Assert.AreEqual(nameof(CouchbaseClusterHealthCheck), result.CheckName);
        }

        [Test]
        public async Task TotalMemoryIsZero_WhenRun_ReturnsError()
        {
            //Arrange
            var goodMemoryRatio = 0.25F;
            var documentsThreshold = 100000;
            var clientMock = new Mock<ICouchbaseClient>();
            var clusterInfo = GetValidClusterDetails(goodMemoryRatio, documentsThreshold);
            clusterInfo.Nodes[0].MemoryTotal = 0;
            clientMock.Setup(m => m.GetClusterDetailsAsync()).ReturnsAsync(clusterInfo);

            var check = new CouchbaseClusterHealthCheck(goodMemoryRatio, documentsThreshold, clientMock.Object);

            //Act
            var result = await check.RunAsync();

            //Assert
            Assert.AreEqual(NotificationLevel.Error, result.Level);
            Assert.AreEqual(nameof(CouchbaseClusterHealthCheck), result.CheckName);
        }

        [Test]
        public async Task DocumentsInfoIsMissing_WhenRun_ReturnsError()
        {
            //Arrange
            var goodMemoryRatio = 0.25F;
            var documentsThreshold = 100000;
            var clientMock = new Mock<ICouchbaseClient>();
            var clusterInfo = GetValidClusterDetails(goodMemoryRatio, documentsThreshold);
            clusterInfo.Nodes[0].InterestingStats = null;
            clientMock.Setup(m => m.GetClusterDetailsAsync()).ReturnsAsync(clusterInfo);

            var check = new CouchbaseClusterHealthCheck(goodMemoryRatio, documentsThreshold, clientMock.Object);

            //Act
            var result = await check.RunAsync();

            //Assert
            Assert.AreEqual(NotificationLevel.Error, result.Level);
            Assert.AreEqual(nameof(CouchbaseClusterHealthCheck), result.CheckName);
        }

        [Test]
        public async Task NotEnoughMemoryReserved_WhenRun_RetunrsWarning()
        {
            //Arrange
            var goodMemoryRatio = 0.25F;
            var documentsThreshold = 100000;
            var clientMock = new Mock<ICouchbaseClient>();
            var clusterInfo = GetValidClusterDetails(goodMemoryRatio, documentsThreshold);
            clusterInfo.Nodes[0].MemoryFree = Convert.ToInt64(clusterInfo.Nodes[0].MemoryTotal * (goodMemoryRatio - 0.2F));
            clientMock.Setup(m => m.GetClusterDetailsAsync()).ReturnsAsync(clusterInfo);

            var check = new CouchbaseClusterHealthCheck(goodMemoryRatio, documentsThreshold, clientMock.Object);

            //Act
            var result = await check.RunAsync();

            //Assert
            Assert.AreEqual(NotificationLevel.Warning, result.Level);
            Assert.AreEqual(nameof(CouchbaseClusterHealthCheck), result.CheckName);
        }

        [Test]
        public async Task TooManyDocuments_WhenRun_RetunrsWarning()
        {
            //Arrange
            var goodMemoryRatio = 0.25F;
            var documentsThreshold = 100000;
            var clientMock = new Mock<ICouchbaseClient>();
            var clusterInfo = GetValidClusterDetails(goodMemoryRatio, documentsThreshold);
            clusterInfo.Nodes[0].InterestingStats.CurrItemsTot++;
            clientMock.Setup(m => m.GetClusterDetailsAsync()).ReturnsAsync(clusterInfo);

            var check = new CouchbaseClusterHealthCheck(goodMemoryRatio, documentsThreshold, clientMock.Object);

            //Act
            var result = await check.RunAsync();

            //Assert
            Assert.AreEqual(NotificationLevel.Warning, result.Level);
            Assert.AreEqual(nameof(CouchbaseClusterHealthCheck), result.CheckName);
        }

        private CouchbaseClusterDetails GetValidClusterDetails(float goodMemoryRatio, int documentsThreshold)
        {
            return new CouchbaseClusterDetails
            {
                ClusterName = "UnitTest",
                Nodes = new List<Node>
                {
                    new Node
                    {
                        MemoryTotal = Convert.ToInt64(8589934592 * goodMemoryRatio),
                        MemoryFree = 8589934592,
                        InterestingStats = new InterestingStats {CurrItemsTot = documentsThreshold}
                    }
                }
            };
        }
    }
}
